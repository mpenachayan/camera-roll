package gal.mpena.cameraroll.themes;

import gal.mpena.cameraroll.R;

public class BlackTheme extends DarkTheme {

    @Override
    public int getBackgroundColorRes() {
        return R.color.black_bg;
    }
}
