package gal.mpena.cameraroll.ui;

import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.transition.Slide;
import android.transition.TransitionSet;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import gal.mpena.cameraroll.R;
import gal.mpena.cameraroll.ui.widget.SwipeBackCoordinatorLayout;
import gal.mpena.cameraroll.util.Util;

public class AboutActivity extends ThemeableActivity
        implements SwipeBackCoordinatorLayout.OnSwipeListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(new Slide(Gravity.BOTTOM));
            getWindow().setReturnTransition(new Slide(Gravity.BOTTOM));
        }

        SwipeBackCoordinatorLayout swipeBackView = findViewById(R.id.swipeBackView);
        swipeBackView.setOnSwipeListener(this);

        final Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final View header = findViewById(R.id.header);
        header.setBackgroundColor(theme.getAccentColor(this));

        TextView version = findViewById(R.id.version);
        try {
            String versionName = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionName;
            final int versionCode = getPackageManager()
                    .getPackageInfo(getPackageName(), 0).versionCode;
            version.setText(Html.fromHtml(versionName));
            version.setTextColor(theme.getAccentTextColor(this));
            version.setOnLongClickListener(view -> {
                Toast.makeText(view.getContext(), "versionCode: " + versionCode, Toast.LENGTH_SHORT).show();
                return false;
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final TextView aboutText = findViewById(R.id.about_text);
        aboutText.setText(Html.fromHtml(getString(R.string.about_text)));
        aboutText.setMovementMethod(new LinkMovementMethod());

        final View rootView = findViewById(R.id.root_view);

        final NestedScrollView scrollView = findViewById(R.id.scroll_view);
        scrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (nestedScrollView, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            int statusBarHeight = toolbar.getPaddingTop();
            if (scrollY > header.getHeight() - statusBarHeight / 2) {
                if (theme.darkStatusBarIcons()) {
                    Util.setDarkStatusBarIcons(rootView);
                } else {
                    Util.setLightStatusBarIcons(rootView);
                }
            } else {
                if (theme.darkStatusBarIconsInSelectorMode()) {
                    Util.setDarkStatusBarIcons(rootView);
                } else {
                    Util.setLightStatusBarIcons(rootView);
                }
            }
        });

        rootView.setOnApplyWindowInsetsListener((view, insets) -> {
            toolbar.setPadding(toolbar.getPaddingStart() /*+ insets.getSystemWindowInsetLeft()*/,
                    toolbar.getPaddingTop() + insets.getSystemWindowInsetTop(),
                    toolbar.getPaddingEnd() /*+ insets.getSystemWindowInsetRight()*/,
                    toolbar.getPaddingBottom());

            aboutText.setPadding(aboutText.getPaddingStart(),
                    aboutText.getPaddingTop(),
                    aboutText.getPaddingEnd(),
                    aboutText.getPaddingBottom() + insets.getSystemWindowInsetBottom());

            View viewGroup = findViewById(R.id.swipeBackView);
            ViewGroup.MarginLayoutParams viewGroupParams
                    = (ViewGroup.MarginLayoutParams) viewGroup.getLayoutParams();
            viewGroupParams.leftMargin += insets.getSystemWindowInsetLeft();
            viewGroupParams.rightMargin += insets.getSystemWindowInsetRight();
            viewGroup.setLayoutParams(viewGroupParams);

            // clear this listener so insets aren't re-applied
            rootView.setOnApplyWindowInsetsListener(null);
            return insets.consumeSystemWindowInsets();
        });

        //set status bar icon color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            rootView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            if (toolbar.getChildAt(i) instanceof ImageView) {
                ImageView imageView = ((ImageView) toolbar.getChildAt(i));
                int color;
                if (!theme.darkStatusBarIconsInSelectorMode()) {
                    color = ContextCompat.getColor(this, R.color.white_translucent1);
                } else {
                    color = ContextCompat.getColor(this, R.color.black_translucent2);
                }
                imageView.setColorFilter(color, PorterDuff.Mode.SRC_IN);
                break;
            }
        }

        //needed for transparent statusBar
        setSystemUiFlags();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean canSwipeBack(int dir) {
        return SwipeBackCoordinatorLayout
                .canSwipeBackForThisView(findViewById(R.id.scroll_view), dir);
    }

    @Override
    public void onSwipeProcess(float percent) {
        getWindow().getDecorView().setBackgroundColor(SwipeBackCoordinatorLayout.getBackgroundColor(percent));
        SwipeBackCoordinatorLayout layout = findViewById(R.id.swipeBackView);
        Toolbar toolbar = findViewById(R.id.toolbar);
        View rootView = findViewById(R.id.root_view);
        int translationY = (int) layout.getTranslationY();
        int statusBarHeight = toolbar.getPaddingTop();
        if (translationY > statusBarHeight * 0.5) {
            if (theme.darkStatusBarIcons()) {
                Util.setDarkStatusBarIcons(rootView);
            } else {
                Util.setLightStatusBarIcons(rootView);
            }
        }
    }

    @Override
    public void onSwipeFinish(int dir) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setReturnTransition(new TransitionSet()
                    .addTransition(new Slide(dir > 0 ? Gravity.TOP : Gravity.BOTTOM))
                    .setInterpolator(new AccelerateDecelerateInterpolator()));
        }
        onBackPressed();
    }

    @Override
    public int getDarkThemeRes() {
        return R.style.CameraRoll_Theme_Translucent_About;
    }

    @Override
    public int getLightThemeRes() {
        return R.style.CameraRoll_Theme_Translucent_Light_About;
    }

    @Override
    public int getTaskDescriptionColor() {
        return accentColor;
    }
}
