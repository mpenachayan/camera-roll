package gal.mpena.cameraroll.adapter.main.viewHolder;

import android.view.View;
import android.widget.ImageView;

import gal.mpena.cameraroll.R;
import gal.mpena.cameraroll.data.models.Album;
import gal.mpena.cameraroll.ui.widget.ParallaxImageView;

public class SimpleAlbumHolder extends AlbumHolder {

    public SimpleAlbumHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void setAlbum(Album album) {
        super.setAlbum(album);
        final ImageView image = itemView.findViewById(R.id.image);
        if (image instanceof ParallaxImageView) {
            ((ParallaxImageView) image).setParallaxTranslation();
        }
        loadImage(image);
    }
}
