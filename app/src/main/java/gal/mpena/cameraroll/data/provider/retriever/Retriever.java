package gal.mpena.cameraroll.data.provider.retriever;

import androidx.appcompat.app.AppCompatActivity;

import gal.mpena.cameraroll.data.provider.MediaProvider;

public abstract class Retriever {

    private MediaProvider.OnMediaLoadedCallback callback;

    public void loadAlbums(final AppCompatActivity context, final boolean hiddenFolders, final MediaProvider.OnMediaLoadedCallback callback) {
        setCallback(callback);
        loadAlbums(context, hiddenFolders);
    }

    abstract void loadAlbums(final AppCompatActivity context, final boolean hiddenFolders);

    public void onDestroy() {
        setCallback(null);
    }

    public void setCallback(MediaProvider.OnMediaLoadedCallback callback) {
        this.callback = callback;
    }

    public MediaProvider.OnMediaLoadedCallback getCallback() {
        return callback;
    }
}
